;;; csexp.scm -- Canonical S-expression reading/writing
;;; Copyright © 2017 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; guile-gcrypt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-gcrypt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.

(define-module (csexp serialize)
  #:use-module (oop goops)
  #:use-module (csexp)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (ice-9 iconv)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9))

(define-inlinable (symbol->bytevector sym)
  (string->utf8 (symbol->string sym)))

(define (symbol->csexp sym)
  "Serialize SYM, optionally with DISPLAY-HINT"
  (csexp-atom (symbol->bytevector sym)))

(define (csexp->symbol csexp)
  "Serialize SYM, optionally with DISPLAY-HINT"
  (string->symbol
   (utf8->string (csexp-atom-octet csexp))))

;; @@: A bit misleading because the string is the one iso-encoded.
;;   But iso-csexp sounds like garbage.
(define (csexp->iso-string csexp)
  "Turn ISO-8859-1 encoded csexp value into a string"
  (bytevector->string (csexp-atom-octet csexp)
                      "ISO-8859-1"))

(define (iso-string->csexp str)
  "Turn string value into an ISO-8859-1 encoded csexp"
  (csexp-atom (string->bytevector str "ISO-8859-1")))

(define (csexp->string csexp)
  "Turn a UTF-8 encoded csexp value into a string"
  (utf8->string (csexp-atom-octet csexp)))

(define (string->csexp str)
  "Turn string value into an csexp"
  (csexp-atom (string->utf8 str)))

(define-class <serializer> ())

(define-class <generic-serializer> (<serializer>)
  (from-csexp-map #:init-thunk make-hash-table
                  #:accessor .from-csexp-map)
  (to-csexp-generic #:init-thunk
                    (lambda ()
                      (define generic
                        (make-generic 'serializer))
                      ;; default, which raises an exception
                      (add-method! generic
                                   (method (obj serializer)
                                           (throw 'no-serializer
                                                  "No seralizer found for obj"
                                                  #:obj obj)))
                      generic)
                     #:accessor .to-csexp-generic)
  ;; (to-csexp-preds #:init-value '()
  ;;                  #:accessor .to-csexp-preds)
  )

(define-method (serialize (serializer <generic-serializer>) obj)
  ((.to-csexp-generic serializer) obj serializer))

(define-method (deserialize (serializer <generic-serializer>) csexp)
  (match csexp
    (((? csexp-atom? (= csexp->string tag)) args ...)
     (match (hash-ref (.from-csexp-map serializer) tag)
       (#f (throw 'csexp-error "Deserializer not found for object with this tag"
                  #:tag tag #:args args))
       ((? procedure? deserializer)
        (apply deserializer serializer args))))))

(define (add-generic-serialization! serializer class write-proc)
  (add-method! (.to-csexp-generic serializer)
               (method ((obj class) serializer)
                       (write-proc serializer obj))))

(define (add-generic-deserialization! serializer tag read-proc)
  (hash-set! (.from-csexp-map serializer)
             (match tag
               ((? symbol?)
                (symbol->string tag))
               ((? string?)
                tag))
             read-proc))

;; (define* (simple-serialize-record rtd)
;;   (let ((field-getters 
;;          (map (lambda (fname)
;;                 (record-accessor rtd fname))
;;               (record-type-fields rtd)))
;;         (sym (record-type-name rtd)))
;;     (lambda (record csexp)
;;       (cons sym
;;             (fold (match-lambda*
;;                     ((getter prev)
;;                      (cons (getter record) prev)))
;;                   '()
;;                   field-getters)))))

;; (define* (simple-deserialize-record rtd)
;;   (let ((constructor (record-constructor rtd)))
;;     (lambda (serializer . csexp-fields)
;;       (define fields
;;         (map (lambda (field)
;;                (deserialize serializer field))
;;              csexp-fields))
;;       (apply constructor fields))))


(define basic-serializer
  (make <generic-serializer>))

(define (number->csexp number)
  (iso-string->csexp (number->string number)))
(define (csexp->number csexp)
  (string->number (csexp->iso-string csexp)))

;; numbers
(add-generic-serialization!
 basic-serializer <number>
 (let ((%number-tag (symbol->csexp 'n)))
   (lambda (serializer number)
     (list %number-tag
           (number->csexp number)))))

(add-generic-deserialization!
 basic-serializer 'n
 (lambda (serializer number-csexp)
   (csexp->number number-csexp)))

;; strings
(add-generic-serialization!
 basic-serializer <string>
 (let ((tag (symbol->csexp 's)))
   (lambda (serializer str)
     (list tag (string->csexp str)))))

(add-generic-deserialization!
 basic-serializer "s"
 (lambda (serializer string-csexp)
   (utf8->string (csexp-atom-octet string-csexp))))

;; symbols
(add-generic-serialization!
 basic-serializer <symbol>
 (let ((tag (symbol->csexp 'S)))
   (lambda (serializer sym)
     (list tag (symbol->csexp sym)))))

(add-generic-deserialization!
 basic-serializer "S"
 (lambda (serializer sym-csexp)
   (csexp->symbol sym-csexp)))

;; lists / pairs
(define (pair-or-null? x)
  (or (pair? x) (null? x)))

(add-generic-serialization!
 basic-serializer <pair>
 (let ((list-tag (symbol->csexp 'l))
       (dotted-list-tag (symbol->csexp 'dl)))
   (lambda (serializer lst)
     (define dotted? #f)
     (define converted-list
       (let lp ((lst lst))
         (match lst
           ((item . (? pair-or-null? rest))
            (cons (serialize serializer item) (lp rest)))
           ((item . dotted-tail)
            (set! dotted? #t)
            (list (serialize serializer item) (serialize serializer dotted-tail)))
           (() '()))))
     (if dotted?
         (cons dotted-list-tag converted-list)
         (cons list-tag converted-list)))))

(add-generic-serialization!
 basic-serializer <null>
 (let ((list-tag (symbol->csexp 'l)))
   (lambda (serializer lst)
     '())))

(add-generic-deserialization!
 basic-serializer "l"
 (lambda (serializer . lst-csexp)
   (map (lambda (x) (deserialize serializer x))
        lst-csexp)))

(add-generic-deserialization!
 basic-serializer "dl"
 (lambda (serializer . lst-csexp)
   (let lp ((lst lst-csexp))
     (match lst
       ((item last)
        (cons (deserialize serializer item)
              (deserialize serializer last)))
       ((item rest ...)
        (cons (deserialize serializer item)
              (lp rest)))))))
