;;; csexp.scm -- Canonical S-expression reading/writing
;;; Copyright © 2017 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; guile-gcrypt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-gcrypt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;;; Some code borrowed from sjson.scm / (ice-9 json)
;;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;;;
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;;;; 02110-1301 USA

(define-module (csexp)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 match)
  #:use-module (ice-9 binary-ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 format)
  #:use-module (csexp base64)
  #:export (read-csexp
            write-csexp

            <csexp-atom>
            csexp-atom csexp-atom?
            csexp-atom-octet csexp-atom-display-hint))

(define char-set:ascii-alphabetic
  (string->char-set "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))
(define char-set:pseudo-alphabetic-punctuation
  (string->char-set "-./_:*+="))

(define char-set:token
  (char-set-union char-set:ascii-alphabetic
                  char-set:digit
                  char-set:pseudo-alphabetic-punctuation))
(define char-set:base64
  (char-set-union (string->char-set "+/=")
                  char-set:ascii-alphabetic
                  char-set:digit))

(define (token-str? str)
  (and (> (string-length str) 0)
       ;; it can't start with a digit
       (not (char-set-contains? char-set:digit (string-ref str 0)))
       (string-every char-set:token str)))

(define-syntax-rule (define-char-matcher proc-name char-set)
  (define (proc-name char)
    (char-set-contains? char-set char)))

(define-char-matcher whitespace-char? char-set:whitespace)
(define-char-matcher digit-char? char-set:digit)
(define-char-matcher base64-char? char-set:base64)



(define-record-type <csexp-atom>
  (make-csexp-atom octet display-hint)
  csexp-atom?
  ;; octet bytevector
  (octet csexp-atom-octet)
  ;; a string or #f
  (display-hint csexp-atom-display-hint))

(define* (csexp-atom octet #:optional display-hint)
  "Make a csexp-atom with OCTET and optionally DISPLAY-HINT.

OCTET should already be converted to a bytevector.  For convenience's sake,
DISPLAY-HINT may be a bytevector, symbol, or string."
  (make-csexp-atom
   octet
   (match display-hint
    (#f #f)
    ((? bytevector?)
     display-hint)
    ((? symbol?)
     (symbol->bytevector display-hint))
    ((? string?)
     (string->bytevector display-hint "ISO-8859-1")))))

(set-record-type-printer!
 <csexp-atom>
 (lambda (csexp port)
   (let* ((octet (csexp-atom-octet csexp))
          (str (bytevector->string octet "iso-8859-1"))
          (display-hint (csexp-atom-display-hint csexp))
          (display-hint-str (and display-hint
                                 (bytevector->string display-hint "iso-8859-1"))))
     (display "<csexp:" port)
     (when display-hint
       (format port "[~a]" display-hint-str))
     (if (token-str? str)
         (display str port)
         (format port "|~a|" (base64-encode octet)))
     (display ">" port))))



(define (assert-char port char)
  "Read a character from PORT and throw an invalid JSON error if the
character is not CHAR."
  (unless (eqv? (read-char port) char)
    (throw 'csexp-error "Expecting but did not find char in port"
           #:char char
           #:port port)))

(define (consume-whitespace port)
  "Discard characters from PORT until a non-whitespace character is
encountered.."
  (match (peek-char port)
    ((? eof-object?) *unspecified*)
    ((? whitespace-char?)
     (read-char port)
     (consume-whitespace port))
    (_ *unspecified*)))

(define (read-list port)
  "Read list from PORT"
  (assert-char port #\()
  (consume-whitespace port)
  (if (eqv? #\) (peek-char port))
      (begin
        (read-char port)
        '()) ; empty list
      (let loop ((result (list (read-value port))))
        (consume-whitespace port)
        (match (peek-char port)
          (#\) ; end of array
           (read-char port)
           (reverse result))
          (_ ; read another value
           (loop (cons (read-value port) result)))))))

(define (read-verbatim port)
  (define length
    (string->number
     (list->string
      (reverse
       (let lp ((buffer '()))
         (match (read-char port)
           ((? digit-char? d)
            (lp (cons d buffer)))
           ;; We're done once we hit the colon
           (#\: buffer)
           (_ (throw 'csexp-error
                     "Invalid character while parsing"
                     #:port port))))))))
  (get-bytevector-n port length))

(define (read-verbatim-without-display port)
  (csexp-atom (read-verbatim port) #f))

(define (read-display-hint port)
  ;; @@: Again, I'm not sure this assert-char stuff is really needed...
  ;;   I guess it's a nice safety check, but...
  (assert-char port #\[)
  (let ((display-verbatim (read-verbatim port)))
    (assert-char port #\])
    (consume-whitespace port)
    (csexp-atom (read-verbatim port)
                     display-verbatim)))

(define (read-base64-value port)
  (assert-char port #\{)
  (let ((b64-string
         (list->string
          (let lp ((buffer '()))
            (match (read-char port)
              (#\} (reverse buffer))
              ((? base64-char? c)
               (lp (cons c buffer)))
              ((? eof-object?)
               (throw 'csexp-error
                      "EOF while parsing"
                      #:port port))
              (other-char
               (throw 'csexp-error
                      "Invalid character while parsing"
                      #:port port
                      #:char other-char)))))))
    (call-with-input-string (bytevector->string (base64-decode b64-string)
                                                "iso-8859-1")
      read-value)))

(define (read-value port)
  "Read a csexp value from PORT."
  ;; @@: It would be faster to read whitespace in these procedures
  ;;   instead of duplicating that here and elsewhere
  (consume-whitespace port)
  (match (peek-char port)
    (#\( (read-list port))
    (#\[ (read-display-hint port))
    (#\{ (read-base64-value port))
    ((? digit-char?)
     (read-verbatim-without-display port))
    ((? eof-object?)
     (throw 'csexp-error
            "EOF while parsing"
            #:port port))
    (_
     (throw 'csexp-error
            "Invalid character while parsing"
            #:port port))))

(define (read-csexp port)
  "Read a canonical-sexp from port.

If multiple canonical sexps are available on the port, this procedure
may be called multiple times.

As a side effect, sets the encoding on PORT to
ISO-8859-1 (latin-1), so that reading one character reads one byte."
  (set-port-encoding! port "ISO-8859-1")
  (consume-whitespace port)
  (match (peek-char port)
    ((? eof-object? eof)
     eof)
    (_ (read-value port))))

;; (define (convert-from-csexp csexp transformer))

(define (write-csexp csexp port)
  "Write CSEXP to PORT.

As a side effect, sets the encoding on PORT to
ISO-8859-1 (latin-1), so that reading one character reads one byte."
  (define (display-verbatim bv)
    (display (bytevector-length bv) port)
    (display ":" port)
    (put-bytevector port bv))
  (set-port-encoding! port "ISO-8859-1")
  (match csexp
    ((? pair?)
     (display "(" port)
     (for-each
      (lambda (item)
        (write-csexp item port))
      csexp)
     (display ")" port))
    ((? csexp-atom?)
     (and=> (csexp-atom-display-hint csexp)
            (lambda (dh)
              (display "[" port)
              (display-verbatim dh)
              (display "]" port)))
     (display-verbatim (csexp-atom-octet csexp)))))



